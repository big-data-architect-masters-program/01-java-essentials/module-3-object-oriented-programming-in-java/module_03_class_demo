public class InheritanceDemo {
    public static void main(String[] args) {
        Derive d = new Derive();
        d.show();
        d.staticshow();
        d.x = 10;
        System.out.println(d.x);
        C c = new C();
    }
}

class Base {
    int x;

    void show() {
        System.out.println("This is show");
    }

    static void staticshow() {
        System.out.println("This is static show");
    }
}

class Derive extends Base {

}

class A {
    public A() {
        System.out.println("This is A");
    }
}

class B extends A {
    public B() {
        System.out.println("This is B");
    }
}

class C extends B {
    public C() {
        System.out.println("This is C");
    }
}