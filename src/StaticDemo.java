public class StaticDemo {
    public static void main(String[] args) {
        Counter c1 = new Counter();
        Counter c2 = new Counter();

        //c2.scount = 300; // can set from object
        c1.increase();
        c2.increase();
        c1.increase();
        c1.increase();
        c2.increase();
        c1.increase();


        c1.showCount();
        c2.showCount();
    }
}

class Counter{
    int count;
    static int scount;

    public Counter() {
        count = 1;
        scount = 1;
    }
    void increase(){
        count++;
        scount++;
    }
    void showCount(){
        System.out.println("Count: "+count);
        System.out.println("S-Count: "+scount);
    }

    static void fun(){
        scount++;
        //count++; //Non-static field 'count' cannot be reference from a static context
    }
}