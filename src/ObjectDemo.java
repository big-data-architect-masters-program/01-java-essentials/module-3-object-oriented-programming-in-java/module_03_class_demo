public class ObjectDemo {
    public static void main(String[] args) {
        User user1 = new User();
        System.out.println(user1);
        User user2 = new User();
        System.out.println(user2);
        User user3 = new User("Harward", "94874893", "harward@gmail.com");
        user1.name = "Tony";
        user2.name = "Robin";
        user1.setPhone("012333444");
        user1.email = "tony@gmail.com";
        System.out.println("Name:" + user1.name + " Phone:" + user1.getPhone() + " Email:" + user1.email);
        System.out.println("Name:" + user2.name + " Phone:" + user2.getPhone() + " Email:" + user2.email);
        System.out.println("Name:" + user3.name + " Phone:" + user3.getPhone() + " Email:" + user3.email);
    }
}

class User {
    String name;
    private String phone;
    String email;

    public User() {
        name = "NA";
        phone = "NA";
        email = "NA";
    }

    public User(String name, String phone, String email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}